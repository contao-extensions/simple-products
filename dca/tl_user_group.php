<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */


/**
 * Extend default palette
 */
$GLOBALS['TL_DCA']['tl_user_group']['palettes']['default'] = str_replace('formp;', 'formp;{product_legend},products,productp,productcatp,producttypp;', $GLOBALS['TL_DCA']['tl_user_group']['palettes']['default']);


/**
 * Add fields to tl_user_group
 */
$GLOBALS['TL_DCA']['tl_user_group']['fields']['products'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'foreignKey'				=> 'tl_product_archive.title',
	'eval'							=> array('multiple'=>true),
	'sql'								=> "blob NULL"
);

$GLOBALS['TL_DCA']['tl_user_group']['fields']['productp'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'options'						=> array('create', 'delete'),
	'reference'					=> &$GLOBALS['TL_LANG']['MSC'],
	'eval'							=> array('multiple'=>true),
	'sql'								=> "blob NULL"
);

$GLOBALS['TL_DCA']['tl_user_group']['fields']['productcatp'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'options'						=> array('show', 'create', 'delete'),
	'reference'					=> &$GLOBALS['TL_LANG']['MSC'],
	'eval'							=> array('multiple'=>true),
	'sql'								=> "blob NULL"
);

$GLOBALS['TL_DCA']['tl_user_group']['fields']['producttypp'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'options'						=> array('show', 'create', 'delete'),
	'reference'					=> &$GLOBALS['TL_LANG']['MSC'],
	'eval'							=> array('multiple'=>true),
	'sql'								=> "blob NULL"
);
