<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */

$strTable = 'tl_module';

/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA'][$strTable]['palettes']['product_list'] = '{title_legend},name,headline,type;{config_legend},product_archive,category_jumpTo,numberOfItems,perPage,skipFirst,filter_ignore,sort_fields;{restriction_legend},product_category,product_featured;{template_legend:hide},product_template,customTpl;{image_legend},imgSize,fallback_image;{redirect_legend:hide},jumpTo;{view_legend:hide},perRow,show_count,show_view,show_sort,show_limit;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';

$GLOBALS['TL_DCA'][$strTable]['palettes']['product_reader'] = '{title_legend},name,headline,type;{config_legend},product_archive,category_jumpTo;{list_legend:hide},product_fields;{template_legend:hide},product_template,customTpl;{image_legend},imgSize,fallback_image;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';

$GLOBALS['TL_DCA'][$strTable]['palettes']['product_category_navigation'] = '{title_legend},name,headline,type;{nav_legend},levelOffset,showLevel,changeLevel,redirectParent,backlink_jumpTo;{restriction_legend},product_category,product_featured;{redirect_legend:hide},jumpTo;{template_legend:hide},navigationTpl,customTpl;{image_legend},imgSize;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';

$GLOBALS['TL_DCA'][$strTable]['palettes']['product_category_list'] = '{title_legend},name,headline,type;{config_legend},numberOfItems,perPage,skipFirst,ignoreAutoItem;{restriction_legend},product_category;{redirect_legend:hide},jumpTo;{template_legend:hide},category_template,customTpl;{image_legend},imgSize;{view_legend:hide},perRow;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';

$GLOBALS['TL_DCA'][$strTable]['palettes']['product_newsletter'] = str_replace(',nl_channels', ',product_archive,nl_channels,nl_inherit', $GLOBALS['TL_DCA'][$strTable]['palettes']['subscribe']);

$GLOBALS['TL_DCA'][$strTable]['palettes']['product_request'] = '{title_legend},name,headline,type;{config_legend},product_archive,request_fields,com_disableCaptcha;{email_legend:hide},email_admin,email_user;{redirect_legend:hide},jumpTo;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';

$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'email_admin';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'email_user';

/**
 * Add subpalettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['email_admin'] = 'admin_email,admin_subject,admin_text';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['email_user'] = 'user_subject,user_text';

/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA'][$strTable]['fields']['product_archive'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'options_callback'	=> array('tl_module_product', 'getProductArchives'),
	'eval'							=> array('multiple'=>true),
	'sql'								=> "blob NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['product_category'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'picker',
	'eval'							=> array('multiple'=>true, 'context' => 'dc.tl_product_category'),
	'sql'								=> "blob NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['product_featured'] = array
(
	'default'						=> 'all_items',
	'exclude'						=> true,
	'inputType'					=> 'select',
	'options'						=> array('all_items', 'featured', 'unfeatured'),
	'reference'					=> &$GLOBALS['TL_LANG'][$strTable],
	'eval'							=> array('tl_class'=>'w50'),
	'sql'								=> "varchar(16) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['product_template'] = array
(
	'default'						=> 'product_list',
	'exclude'						=> true,
	'inputType'					=> 'select',
	'options_callback'	=> array('tl_module_product', 'getProductTemplates'),
	'eval'							=> array('tl_class'=>'w50'),
	'sql'								=> "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['filter_ignore'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'eval'							=> array('tl_class'=>'w50 m12'),
	'sql'								=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['show_count'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'eval'							=> array('tl_class'=>'w50 m12'),
	'sql'								=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['show_limit'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'text',
	'eval'							=> array('tl_class'=>'clr'),
	'sql'								=> "text NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['show_view'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'eval'							=> array('tl_class'=>'w50 m12'),
	'sql'								=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['email_admin'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'eval'							=> array('submitOnChange'=>true),
	'sql'								=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['admin_email'] = array
(
	'inputType'					=> 'text',
	'exclude'						=> true,
	'eval'							=> array('tl_class' => 'long'),
	'sql'								=> "text NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['admin_subject'] = array
(
	'exclude'						=> true,
	'search'						=> true,
	'inputType'					=> 'text',
	'eval'							=> array('mandatory'=>true, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'long'),
	'sql'								=> "text NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['admin_text'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'textarea',
	'eval'							=> array('style'=>'height:120px', 'decodeEntities'=>true, 'alwaysSave'=>true, 'allowHtml' => true, 'rte' => 'tinyMCE'),
	'sql'								=> "text NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['email_user'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'eval'							=> array('submitOnChange'=>true),
	'sql'								=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['user_subject'] = array
(
	'exclude'						=> true,
	'search'						=> true,
	'inputType'					=> 'text',
	'eval'							=> array('mandatory'=>true, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'long'),
	'sql'								=> "text NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['user_text'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'textarea',
	'eval'							=> array('style'=>'height:120px', 'decodeEntities'=>true, 'alwaysSave'=>true, 'allowHtml' => true, 'rte' => 'tinyMCE'),
	'sql'								=> "text NULL"
);

if (!array_key_exists('sort_fields', $GLOBALS['TL_DCA'][$strTable]['fields']))
{
	$GLOBALS['TL_DCA'][$strTable]['fields']['sort_fields'] = array
	(
		'exclude'						=> true,
		'inputType'					=> 'checkboxWizard',
		'eval'							=> array('multiple'=>true, 'tl_class'=>'clr'),
		'sql'								=> "blob NULL"
	);
}

if (!array_key_exists('show_sort', $GLOBALS['TL_DCA'][$strTable]['fields']))
{
	$GLOBALS['TL_DCA'][$strTable]['fields']['show_sort'] = array
	(
		'exclude'						=> true,
		'inputType'					=> 'checkboxWizard',
		'eval'							=> array('multiple'=>true, 'tl_class'=>'clr'),
		'sql'								=> "blob NULL"
	);
}

if (TL_MODE == 'BE')
{
	if(\Input::get('table') == $strTable && \Input::get('act') == 'edit' && \Input::get('id'))
	{
		$strModuleType = \SimpleProducts\ModuleModel::findById(\Input::get('id'))->type;
		// fields for event manager modules only
		if($strModuleType == 'product_list' || $strModuleType == 'productlist_booking')
		{
			$GLOBALS['TL_DCA'][$strTable]['fields']['sort_fields']['options_callback'] = array('tl_module_product', 'getSortFields');
			$GLOBALS['TL_DCA'][$strTable]['fields']['show_sort']['options_callback'] = array('tl_module_product', 'getSortFields');
		}
		if($strModuleType == 'product_request')
		{
			$GLOBALS['TL_DCA'][$strTable]['fields']['request_fields']['options_callback'][] = array('tl_module_product', 'getRequestFields');

			$GLOBALS['TL_DCA'][$strTable]['fields']['admin_subject']['load_callback'][] = array('tl_module_product', 'getRequestSubjectDefault');
			$GLOBALS['TL_DCA'][$strTable]['fields']['admin_text']['load_callback'][] = array('tl_module_product', 'getRequestTextDefault');
			$GLOBALS['TL_DCA'][$strTable]['fields']['admin_text']['eval']['rte'] = '';

			$GLOBALS['TL_DCA'][$strTable]['fields']['user_subject']['load_callback'][] = array('tl_module_product', 'getRequestSubjectDefault');
			$GLOBALS['TL_DCA'][$strTable]['fields']['user_text']['load_callback'][] = array('tl_module_product', 'getRequestTextDefault');
			$GLOBALS['TL_DCA'][$strTable]['fields']['user_text']['eval']['rte'] = '';
		}
	}
}

$GLOBALS['TL_DCA'][$strTable]['fields']['product_fields'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkboxWizard',
	'options_callback'  => array('tl_module_product', 'getListFields'),
	'eval'							=> array('multiple'=>true, 'tl_class'=>'clr'),
	'sql'								=> "blob NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['category_template'] = array
(
	'default'						=> 'category_list',
	'exclude'						=> true,
	'inputType'					=> 'select',
	'options_callback'	=> array('tl_module_product', 'getCategoryTemplates'),
	'eval'							=> array('tl_class'=>'w50'),
	'sql'								=> "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['ignoreAutoItem'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'eval'							=> array('tl_class'=>'w50 m12'),
	'sql'								=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['category_jumpTo'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'pageTree',
	'foreignKey'				=> 'tl_page.title',
	'eval'							=> array('fieldType'=>'radio'),
	'sql'								=> "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['backlink_jumpTo'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'pageTree',
	'foreignKey'		  	=> 'tl_page.title',
	'eval'							=> array('fieldType'=>'radio', 'tl_class'=>'clr'),
	'sql'								=> "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['fallback_image'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'fileTree',
	'eval'							=> array('tl_class'=>'clr', 'fieldType' => 'radio', 'files' => true, 'filesOnly' => true),
	'sql'								=> "binary(16) NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['perRow'] = array
(
	'default'						=> 1,
	'exclude'						=> true,
	'inputType'					=> 'select',
	'options'						=> array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
	'eval'							=> array('tl_class'=>'w50'),
	'sql'								=> "smallint(5) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['nl_inherit'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'select',
	'options'						=> array('', 'product'),
	'reference'					=> array('' => 'Newsletter von Modul und Produkt', 'product' => 'Nur Newsletter vom Produkt'),
	'eval'							=> array('tl_class'=>'clr'),
	'sql'								=> "varchar(16) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['changeLevel'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkbox',
	'eval'							=> array('tl_class'=>'w50 m12'),
	'sql'								=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['request_fields'] = array
(
	'exclude'						=> true,
	'inputType'					=> 'checkboxWizard',
	'options_callback'	=> array('tl_module_product', 'getRequestFields'),
	'eval'							=> array('mandatory'=>true, 'multiple'=>true, 'tl_class'=>'clr'),
	'sql'								=> "blob NULL"
);

/**
 * Add the comments template drop-down menu
 */
if (in_array('comments', ModuleLoader::getActive()))
{
	$GLOBALS['TL_DCA']['tl_module']['palettes']['product_reader'] = str_replace('{protected_legend:hide}', '{comment_legend:hide},com_template;{protected_legend:hide}', $GLOBALS['TL_DCA']['tl_module']['palettes']['product_reader']);
}


class tl_module_product extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}

	/**
	 * Get all product archives and return them as array
	 * @return array
	 */
	public function getProductArchives()
	{
		if (!$this->User->isAdmin && !is_array($this->User->news))
		{
			return array();
		}

		$arrArchives = array();
		$objArchives = \Database::getInstance()->execute("SELECT id, title FROM tl_product_archive ORDER BY title");

		while ($objArchives->next())
		{
			if ($this->User->isAdmin || $this->User->hasAccess($objArchives->id, 'product'))
			{
				$arrArchives[$objArchives->id] = $objArchives->title;
			}
		}

		return $arrArchives;
	}

	/**
	 * Get all product reader modules and return them as array
	 * @return array
	 */
	public function getReaderModules()
	{
		$arrModules = array();
		$objModules = \Database::getInstance()->execute("SELECT m.id, m.name, t.name AS theme FROM tl_module m LEFT JOIN tl_theme t ON m.pid=t.id WHERE m.type='productreader' ORDER BY t.name, m.name");

		while ($objModules->next())
		{
			$arrModules[$objModules->theme][$objModules->id] = $objModules->name . ' (ID ' . $objModules->id . ')';
		}

		return $arrModules;
	}

	/**
	 * Return all product templates as array
	 * @return array
	 */
	public function getProductTemplates()
	{
		return $this->getTemplateGroup('product_');
	}

	/**
	 * Return all product templates as array
	 * @return array
	 */
	public function getCategoryTemplates()
	{
		return $this->getTemplateGroup('category_');
	}

	/**
	 * Return all mandatory fields of table tl_member
	 * @return array
	 */
	public function getSortFields()
	{
		\System::loadLanguageFile('tl_product');
		$this->loadDataContainer('tl_product');

		foreach ($GLOBALS['TL_DCA']['tl_product']['fields'] AS $key=>$field)
		{
			if($GLOBALS['TL_DCA']['tl_product']['fields'][$key]['sorting'] == true)
			{
				$arrFields[$key.' asc'] = $GLOBALS['TL_DCA']['tl_product']['fields'][$key]['label'][0].': '.$GLOBALS['TL_LANG']['product']['search']['asc'].' <span style="color:#bbb;display:inline;margin-left:3px">['.$key.' asc]</span>';
				$arrFields[$key.' desc'] = $GLOBALS['TL_DCA']['tl_product']['fields'][$key]['label'][0].': '.$GLOBALS['TL_LANG']['product']['search']['desc'].' <span style="color:#bbb;display:inline;margin-left:3px">['.$key.' desc]</span>';
			}

			$arrFields['rand'] = 'Zufall <span style="color:#bbb;display:inline;margin-left:3px">[rand]</span>';
		}

		return $arrFields;
	}

	/**
	 * Return all mandatory fields of table tl_member
	 * @return array
	 */
	public function getListFields()
	{
		System::loadLanguageFile('tl_product');
		$this->loadDataContainer('tl_product');

		foreach ($GLOBALS['TL_DCA']['tl_product']['fields'] AS $key=>$field)
		{
			if($GLOBALS['TL_DCA']['tl_product']['fields'][$key]['eval']['feViewable'] == true)
			{
				$arrFields[$key] = $GLOBALS['TL_DCA']['tl_product']['fields'][$key]['label'][0].' <span style="color:#bbb;display:inline;margin-left:3px">['.$key.']</span>';
			}
		}

		return $arrFields;
	}

	/**
	 * Return all public fields of table tl_guestbook_entry
	 * @return array
	 */
	public function getRequestFields()
	{

		System::loadLanguageFile('tl_product_request');
		$this->loadDataContainer('tl_product_request');

		foreach ($GLOBALS['TL_DCA']['tl_product_request']['fields'] AS $key=>$field)
		{
			if($GLOBALS['TL_DCA']['tl_product_request']['fields'][$key]['eval']['feEditable'] == true)
			{
				$arrFields[$key] = $GLOBALS['TL_DCA']['tl_product_request']['fields'][$key]['label'][0].' <span style="color:#bbb;display:inline;margin-left:3px">['.$key.']</span>';
			}
		}

		return $arrFields;
	}


	/**
	 * Load the default admin text
	 * @param mixed
	 * @return mixed
	 */
	public function getRequestTextDefault($varValue)
	{
		if (!trim($varValue))
		{
			$varValue = (is_array($GLOBALS['TL_LANG']['product_request']['email']['admin_text']) ? $GLOBALS['TL_LANG']['product_request']['email']['admin_text'][1] : $GLOBALS['TL_LANG']['product_request']['email']['admin_text']);
		}

		return $varValue;
	}

	/**
	 * Load the default admin text
	 * @param mixed
	 * @return mixed
	 */
	public function getRequestSubjectDefault($varValue)
	{
		if (!trim($varValue))
		{
			$varValue = (is_array($GLOBALS['TL_LANG']['product_request']['email']['admin_subject']) ? $GLOBALS['TL_LANG']['product_request']['email']['admin_subject'][1] : $GLOBALS['TL_LANG']['product_request']['email']['admin_subject']);
		}

		return $varValue;
	}
}
