<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace SimpleProducts;


class ProductListFeatures extends \ContentList
{

	public function __construct($arrItems)
	{
		$this->id = $objTemplate->id;
		$this->type = 'list feature_list';
		$this->tstamp = time();

		// Apply product feature list
		$this->listitems = $arrItems;
	}
}
