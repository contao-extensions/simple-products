<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['product'] = array('Produkte', 'Produkte verwalten');
$GLOBALS['TL_LANG']['MOD']['product_categories'] = array('Produktkategorien');

/**
 * Extension folder
 */
$GLOBALS['TL_LANG']['MOD']['simple_products'] = array('Produktkatalog');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['simple_products'] = 'Produkte';
$GLOBALS['TL_LANG']['FMD']['product_list'] = array('Produktliste', 'Fügt der Seite eine Produktliste hinzu.');
$GLOBALS['TL_LANG']['FMD']['product_reader'] = array('Produktleser', 'Stellt ein einzelnes Produkt dar.');
$GLOBALS['TL_LANG']['FMD']['product_category_navigation'] = array('Navigation für Produktkategorien', 'Fügt der Seite eine Navigation mit Produktkategorien hinzu.');
$GLOBALS['TL_LANG']['FMD']['product_category_list'] = array('Auflistung von Produktkategorien', 'Fügt der Seite eine Liste von Produktkategorien hinzu.');
$GLOBALS['TL_LANG']['FMD']['product_request'] = array('Produktanfrage', 'Fügen Sie auf der Seite des Produktlesers ein Formular für eine Anfrage ein.');
$GLOBALS['TL_LANG']['FMD']['product_newsletter'] = array('Produkt-Newsletter', 'Wird auf der gleichen Seite wie der Produktleser eingebunden und stellt nur die Newsletter dar, die dem Produkt zugewiesen wurden');
