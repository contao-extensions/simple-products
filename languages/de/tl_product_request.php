<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */

$strTable = 'tl_product_request';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['name'] = array('Name', 'Der Name der Person, die die Anfrage erstellt hat.');
$GLOBALS['TL_LANG'][$strTable]['gender'] = array('Anrede', 'Bitte wählen Sie die Anrede.');
$GLOBALS['TL_LANG'][$strTable]['company'] = array('Firma', 'Der Firmenname der Person, die die Anfrage erstellt hat.');
$GLOBALS['TL_LANG'][$strTable]['email'] = array('E-Mail', 'Die E-Mail-Adresse der Person, die die Anfrage erstellt hat.');
$GLOBALS['TL_LANG'][$strTable]['comment'] = array('Ihre Fragen / Nachricht', 'Die vollständige Nachricht der Anfrage.');
$GLOBALS['TL_LANG'][$strTable]['title'] = array('Betreff', 'Das Thema der Anfrage.');
$GLOBALS['TL_LANG'][$strTable]['phone'] = array('Telefon', 'Die Telefonnummer der Person, die die Anfrage erstellt hat.');
$GLOBALS['TL_LANG'][$strTable]['skype'] = array('Skype', 'Skype-Name der Person, die die Anfrage erstellt hat.');

$GLOBALS['TL_LANG'][$strTable]['privacy'] = array('Datenschutz', 'Ich stimme zu, dass meine Angaben aus dem Anfrageformular zur Beantwortung meiner Anfrage erhoben und verarbeitet werden. Die Daten werden spätestens nach Ablauf der gesetzlichen Verjährungsfrist gelöscht. Hinweis: Sie können Ihre Einwilligung jederzeit mit Wirkung für die Zukunft per E-Mail widerrufen. Detaillierte Informationen zum Umgang mit Nutzerdaten finden Sie in unserer Datenschutzerklärung.');

$GLOBALS['TL_LANG'][$strTable]['date'] = array('Datum', 'Das Erstellungsdatum der Anfrage.');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['main_legend'] = 'Allgemeines';
$GLOBALS['TL_LANG'][$strTable]['comment_legend'] = 'Nachricht';
$GLOBALS['TL_LANG'][$strTable]['author_legend'] = 'Autor';

/**
 * Buttons
 */
$GLOBALS['TL_LANG'][$strTable]['edit'] = array('Anfrage bearbeiten', 'Anfrage ID %s bearbeiten');
$GLOBALS['TL_LANG'][$strTable]['delete'] = array('Anfrage löschen', 'Anfrage ID %s löschen');
$GLOBALS['TL_LANG'][$strTable]['show'] = array('Anfrage Details', 'Details von Anfrage ID %s anzeigen');
