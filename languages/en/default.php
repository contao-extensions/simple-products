<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */


/**
 * Miscellaneous
 */
$GLOBALS['TL_LANG']['MSC']['show'] = 'Show';
$GLOBALS['TL_LANG']['MSC']['emptyProductList'] = 'In this range are currently no products available.';
$GLOBALS['TL_LANG']['MSC']['emptyCategoryList'] = 'There are no subcategories in this section.';
$GLOBALS['TL_LANG']['MSC']['moreProduct'] = 'Product details';
$GLOBALS['TL_LANG']['MSC']['moreCategory'] = 'View products';
$GLOBALS['TL_LANG']['MSC']['readMoreProduct'] = 'Find out more about the product \'%s\'';
$GLOBALS['TL_LANG']['MSC']['readMoreCategory'] = 'View all products of the category %s';
$GLOBALS['TL_LANG']['MSC']['invalidProduct'] = 'The product does not exist or has been removed.';

$GLOBALS['TL_LANG']['CUR']['CHF'] = 'Fr.';
$GLOBALS['TL_LANG']['CUR']['DKK'] = 'Kr.';
$GLOBALS['TL_LANG']['CUR']['EUR'] = '€';
$GLOBALS['TL_LANG']['CUR']['GBP'] = '£';
$GLOBALS['TL_LANG']['CUR']['JPY'] = '¥';
$GLOBALS['TL_LANG']['CUR']['NOK'] = 'Kr.';
$GLOBALS['TL_LANG']['CUR']['SEK'] = 'Kr.';
$GLOBALS['TL_LANG']['CUR']['USD'] = '$';

$GLOBALS['TL_LANG']['MSC']['product_noprice'] = array
(
	'request'	=> 'On request',
	'none'	=> 'Show no price',
	'free'	=> 'Free'
);

$GLOBALS['TL_LANG']['product']['search'] = array
(
	'asc'	=> 'ascending',
	'desc'	=> 'descending'
);

$GLOBALS['TL_LANG']['product_list']['list_sort']['sort'] = 'Sort by';
$GLOBALS['TL_LANG']['product_list']['list_sort']['sorting'] = 'Relevance';

$GLOBALS['TL_LANG']['product_list']['list_view']['view'] = 'View';
$GLOBALS['TL_LANG']['product_list']['list_view']['list'] = array('list', 'switch to list view');
$GLOBALS['TL_LANG']['product_list']['list_view']['gallery'] = array('gallery', 'switch to gallery view');

$GLOBALS['TL_LANG']['product_info']['tax'] = 'incl. VAT';
$GLOBALS['TL_LANG']['product_info']['tax_list'] = 'All prices include VAT';
$GLOBALS['TL_LANG']['product_info']['tax_small_business'] = 'Final price, no VAT shown according to § 19 UStG';
$GLOBALS['TL_LANG']['product_info']['tax_list_small_business'] = 'The given prices are final prices. Due to the small business regulation according to § 19 UStG, no sales tax is shown.';

$GLOBALS['TL_LANG']['product_request']['captcha'] = 'Security code';
$GLOBALS['TL_LANG']['product_request']['submit'] = 'Send request';
$GLOBALS['TL_LANG']['product_request']['success'] = 'Thank you for your request.';

$GLOBALS['TL_LANG']['product_request']['email']['admin_subject'] = '[New Product Request] {{product_title}}';
$GLOBALS['TL_LANG']['product_request']['email']['admin_text'] = 'Dear Sir or Madam,

A new request has been sent for an item on your website:
{{product_url}}

---

{{comment}}

---

From {{name}}';