<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */

$strTable = 'tl_product_request';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['name'] = array('Name', 'The full name of the requester.');
$GLOBALS['TL_LANG'][$strTable]['gender'] = array('Salutation', 'Please select the salutation.');
$GLOBALS['TL_LANG'][$strTable]['company'] = array('Company', 'The company name of the requester.');
$GLOBALS['TL_LANG'][$strTable]['email'] = array('E-mail', 'The email address of the requester.');
$GLOBALS['TL_LANG'][$strTable]['comment'] = array('Message', 'The full message of the request.');
$GLOBALS['TL_LANG'][$strTable]['title'] = array('Title', 'The subject of the request.');
$GLOBALS['TL_LANG'][$strTable]['phone'] = array('Phone', 'The phone number of the person who created the request.');
$GLOBALS['TL_LANG'][$strTable]['skype'] = array('Skype', 'The Skype name of the person who created the request.');

$GLOBALS['TL_LANG'][$strTable]['privacy'] = array('Privacy', 'I agree that my details from the request form will be collected and processed to answer my request. The data will be deleted at the latest after the statutory limitation period has expired. Note: You can revoke your consent at any time by email with effect for the future. Detailed information on handling user data can be found in our data protection declaration.');

$GLOBALS['TL_LANG'][$strTable]['date'] = array('Date', 'The creation date of the request.');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['main_legend'] = 'General';
$GLOBALS['TL_LANG'][$strTable]['comment_legend'] = 'Message';
$GLOBALS['TL_LANG'][$strTable]['author_legend'] = 'Author';

/**
 * Buttons
 */
$GLOBALS['TL_LANG'][$strTable]['edit'] = array('Kommentar bearbeiten', 'Kommentar ID %s bearbeiten');
$GLOBALS['TL_LANG'][$strTable]['delete'] = array('Kommentar löschen', 'Kommentar ID %s löschen');
$GLOBALS['TL_LANG'][$strTable]['show'] = array('Kommentar Details', 'Details von Kommentar ID %s anzeigen');
