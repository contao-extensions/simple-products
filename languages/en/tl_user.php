<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */

$strTable = 'tl_user';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['products'] = array('Erlaubte Archive', 'Hier können Sie den Zugriff auf ein oder mehrere Produkt-Archive erlauben.');
$GLOBALS['TL_LANG'][$strTable]['productp'] = array('Archivrechte', 'Hier können Sie die Archivrechte festlegen.');
$GLOBALS['TL_LANG'][$strTable]['productcatp'] = array('Rechte für Produktkategorien', 'Hier können Sie die Berechtigungen für die Produktkategorien festlegen.');
$GLOBALS['TL_LANG'][$strTable]['producttypp'] = array('Rechte für Produktarten', 'Hier können Sie die Berechtigungen für die Produktarten festlegen.');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['product_legend'] = 'Produkt-Rechte';
