<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */

$strTable = 'tl_user_group';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['products'] = array('Разрешенные архивы', 'Здесь вы можете разрешить доступ к одному или нескольким архивам товаров.');
$GLOBALS['TL_LANG'][$strTable]['productp'] = array('Права на архив', 'Здесь вы можете задать права на архив.');
$GLOBALS['TL_LANG'][$strTable]['productcatp'] = array('Права на категории товаров', 'Здесь можно задать разрешения для категорий птоваров.');
$GLOBALS['TL_LANG'][$strTable]['producttypp']	= array('Права на типы товаров', 'Здесь можно задать разрешения для типов товаров.');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['product_legend'] = 'Права-товаров';
