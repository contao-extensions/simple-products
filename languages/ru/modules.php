<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['product'] = array('Магазин', 'Управление товарами');
$GLOBALS['TL_LANG']['MOD']['product_categories'] = array('Категории товаров');

/**
 * Extension folder
 */
$GLOBALS['TL_LANG']['MOD']['simple_products'] = array('Каталог товаров');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['simple_products'] = 'Товары';
$GLOBALS['TL_LANG']['FMD']['productlist'] = array('Список товаров', 'Добавляет список товаров на страницу.');
$GLOBALS['TL_LANG']['FMD']['productreader'] = array('Показать товар', 'Представляет один товар.');
$GLOBALS['TL_LANG']['FMD']['productcategories'] = array('Навигация по категориям товаров', 'Добавьте навигацию к категориям товаров на странице.');
$GLOBALS['TL_LANG']['FMD']['product_categorylist'] = array('Список категорий товаров', 'Добавляет список категорий товаров на страницу.');
$GLOBALS['TL_LANG']['FMD']['product_request'] = array('Produktanfrage', 'Включите форму для запроса на странице просмотра товара.');
$GLOBALS['TL_LANG']['FMD']['product_newsletter'] = array('Новостная рассылка товаров', 'Включается на той же странице, что и просмотр товара, и отображает только те информационные бюллетени, которые были назначены товару.');
