<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */

$strTable = 'tl_module';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['product_archive'] = array('Архив товаров', 'Пожалуйста, выберите один или несколько архивов товаров.');
$GLOBALS['TL_LANG'][$strTable]['product_category'] = array('Категории товаров', 'Вы можете указать, что модуль учитывает только определенные категории. Если категория Не выбрана,учитываются все категории.');
$GLOBALS['TL_LANG'][$strTable]['category_jumpTo'] = array('Страница переадресации для категорий', 'Необходимо только в том случае, если вы хотите отобразить категории в списке товаров. Укажите, на какую страницу будет перенаправляться посетитель при нажатии на категорию.');
$GLOBALS['TL_LANG'][$strTable]['backlink_jumpTo'] = array('Обратная Ссылка', 'Выберите родительскую страницу, например, если вы хотите установить обратную ссылку на обзор товара при навигации.');
$GLOBALS['TL_LANG'][$strTable]['product_featured'] = array('Выделенные товары', 'Здесь вы определяете, как обрабатываются выделенные товары.');
$GLOBALS['TL_LANG'][$strTable]['product_template'] = array('Шаблон товара', 'Здесь вы можете выбрать шаблон товара.');

$GLOBALS['TL_LANG'][$strTable]['perRow'] = array('Элементы в строке', 'Требуется настройка ваших настроек CSS. Если вы выберете эту опцию, исходный код будет сгенерирован таким образом, что элементы CSS могут отображаться рядом. Последний элемент в строке получает класс "last".');

$GLOBALS['TL_LANG'][$strTable]['fallback_image'] = array('Изображение по умолчанию', 'Это изображение отображается, если для соответствующего элемента не существует изображения.');

$GLOBALS['TL_LANG'][$strTable]['product_fields'] = array('Список полей товара', 'Выберите поля и порядок, если вы просто хотите перечислить много полей.');

$GLOBALS['TL_LANG'][$strTable]['filter_ignore'] = array('Игнорировать фильтры', 'При выборе этого параметра параметры URL для фильтрации игнорируются (категория, поиск товаров).');

$GLOBALS['TL_LANG'][$strTable]['sort_fields'] = array('Сортировка По умолчанию', 'Выберите, как результаты должны быть отсортированы. По умолчанию результаты сортируются по индексу сортировки.');

$GLOBALS['TL_LANG'][$strTable]['show_sort'] = array('Сортировать по (Selectbox)', 'Если вы выберете что-то здесь, поле выбора будет отображаться над списком продуктов для сортировки результатов.');
$GLOBALS['TL_LANG'][$strTable]['show_limit'] = array('Результатов на страницу (Selectbox)', 'Если вы выберете что-то здесь, поле со списком будет отображаться над списком продуктов, чтобы изменить результаты на странице.');
$GLOBALS['TL_LANG'][$strTable]['show_view'] = array('Настройка внешнего вида (Раскрывающееся меню)', 'Если вы выберете эту опцию, посетитель вашего сайта будет иметь возможность изменить внешний вид списка над списком продуктов (галерея/список).');

$GLOBALS['TL_LANG'][$strTable]['email_admin']	= array('Отправить письмо администратору', 'Выберите эту опцию, чтобы отправить запрос администратору сайта по электронной почте.');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['list_legend'] = 'Список';
$GLOBALS['TL_LANG'][$strTable]['view_legend'] = 'Просмотр настроек в фронтенде';
$GLOBALS['TL_LANG'][$strTable]['restriction_legend'] = 'Ограничения';
