<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['product'] = array('Products', 'Manage products');
$GLOBALS['TL_LANG']['MOD']['product_categories'] = array('Product categories');

/**
 * Extension folder
 */
$GLOBALS['TL_LANG']['MOD']['simple_products'] = array('Product catalog');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['simple_products'] = 'Products';
$GLOBALS['TL_LANG']['FMD']['productlist'] = array('Product list', 'Adds a list of product items to the page.');
$GLOBALS['TL_LANG']['FMD']['productreader'] = array('Product reader', 'Shows the details of a product item.');
$GLOBALS['TL_LANG']['FMD']['productcategories'] = array('Navigation for Product categories', 'Generates a navigation menu to show the product categories.');
$GLOBALS['TL_LANG']['FMD']['product_categorylist'] = array('Auflistung von Produktkategorien', 'Fügt der Seite eine Liste von Produktkategorien hinzu.');
$GLOBALS['TL_LANG']['FMD']['product_request'] = array('Produktanfrage', 'Fügen Sie auf der Seite des Produktlesers ein Formular für eine Anfrage ein.');
$GLOBALS['TL_LANG']['FMD']['product_newsletter'] = array('Produkt-Newsletter', 'Wird auf der gleichen Seite wie der Produktleser eingebunden und stellt nur die Newsletter dar, die dem Produkt zugewiesen wurden');
