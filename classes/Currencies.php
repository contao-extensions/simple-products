<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace SimpleProducts;


class Currencies extends \Controller
{

	static function getCurrency($value='')
	{
		/**
		 * Currencies
		 */
		$currencies = array
		(
			"ad" => "EUR",
			"ae" => "AED",
			"af" => "AFN",
			"ag" => "USD",
			"ai" => "USD",
			"al" => "ALL",
			"am" => "AMD",
			"an" => "ANG",
			"ao" => "AOA",
			"ar" => "ARS",
			"as" => "USD",
			"at" => "EUR",
			"au" => "AUD",
			"aw" => "AWG",
			"az" => "AZN",
			"ba" => "BAM",
			"bb" => "BBD",
			"bd" => "BDT",
			"be" => "EUR",
			"bf" => "XOF",
			"bg" => "BGN",
			"bh" => "BHD",
			"bi" => "BIF",
			"bj" => "XOF",
			"bm" => "BMD",
			"bn" => "BND",
			"bo" => "BOB",
			"br" => "BRL",
			"bs" => "BSD",
			"bt" => "BTN",
			"bv" => "NOK",
			"bw" => "BWP",
			"by" => "BYR",
			"bz" => "BZD",
			"ca" => "CAD",
			"cc" => "AUD",
			"cd" => "CDF",
			"cf" => "XAF",
			"cg" => "XAF",
			"ch" => "CHF",
			"ci" => "XOF",
			"ck" => "NZD",
			"cl" => "CLP",
			"cm" => "XAF",
			"cn" => "CNY",
			"co" => "COP",
			"cr" => "CRC",
			"cs" => "CSJ",
			"cu" => "CUC",
			"cv" => "CVE",
			"cx" => "AUD",
			"cy" => "EUR",
			"cz" => "CZK",
			"dd" => "DDM",
			"de" => "EUR",
			"dj" => "DJF",
			"dk" => "DKK",
			"dm" => "XCD",
			"do" => "DOP",
			"dz" => "DZD",
			"ec" => "USD",
			"ee" => "EUR",
			"eg" => "EGP",
			"eh" => "MAD",
			"er" => "ERN",
			"es" => "EUR",
			"et" => "ETB",
			"fi" => "EUR",
			"fj" => "FJD",
			"fk" => "FKP",
			"fm" => "USD",
			"fo" => "DKK",
			"fr" => "EUR",
			"ga" => "XAF",
			"gb" => "GBP",
			"gd" => "XCD",
			"ge" => "GEL",
			"gf" => "EUR",
			"gh" => "GHS",
			"gi" => "GIP",
			"gl" => "DKK",
			"gm" => "GMD",
			"gn" => "GNF",
			"gp" => "EUR",
			"gq" => "EQE",
			"gr" => "EUR",
			"gt" => "GTQ",
			"gu" => "USD",
			"gw" => "XAF",
			"gy" => "GYD",
			"hk" => "HKD",
			"hm" => "AUD",
			"hn" => "HNL",
			"hr" => "HRK",
			"ht" => "HTG",
			"hu" => "HUF",
			"id" => "IDR",
			"ie" => "EUR",
			"il" => "ILS",
			"in" => "INR",
			"io" => "GBP",
			"iq" => "IQD",
			"ir" => "IRR",
			"is" => "ISK",
			"it" => "EUR",
			"jm" => "JMD",
			"jo" => "JOD",
			"jp" => "JPY",
			"ke" => "KES",
			"kg" => "KGS",
			"kh" => "KHR",
			"ki" => "AUD",
			"km" => "KMF",
			"kn" => "XCD",
			"kp" => "KPW",
			"kr" => "KRW",
			"kw" => "KWD",
			"ky" => "KYD",
			"kz" => "KZT",
			"la" => "LAK",
			"lb" => "LBP",
			"lc" => "XCD",
			"li" => "CHF",
			"lk" => "LKR",
			"lr" => "LRD",
			"ls" => "LSL",
			"lt" => "LTL",
			"lu" => "EUR",
			"lv" => "LVL",
			"ly" => "LYD",
			"ma" => "MAD",
			"mc" => "EUR",
			"md" => "MDL",
			"me" => "EUR",
			"mg" => "MGA",
			"mh" => "USD",
			"mk" => "MKD",
			"ml" => "XOF",
			"mm" => "MMK",
			"mn" => "MNT",
			"mo" => "MOP",
			"mp" => "USD",
			"mq" => "EUR",
			"mr" => "MRO",
			"ms" => "XCD",
			"mt" => "EUR",
			"mu" => "MUR",
			"mv" => "MVR",
			"mw" => "MWK",
			"mx" => "MXN",
			"my" => "MYR",
			"mz" => "MZN",
			"na" => "NAD",
			"nc" => "XPF",
			"ne" => "XOF",
			"nf" => "AUD",
			"ng" => "NGN",
			"ni" => "NIO",
			"nl" => "EUR",
			"no" => "NOK",
			"np" => "NPR",
			"nr" => "AUD",
			"nu" => "NZD",
			"nz" => "NZD",
			"om" => "OMR",
			"pa" => "PAB",
			"pe" => "PEN",
			"pf" => "XPF",
			"pg" => "PGK",
			"ph" => "PHP",
			"pk" => "PKR",
			"pl" => "PLN",
			"pm" => "EUR",
			"pn" => "NZD",
			"pr" => "USD",
			"pt" => "EUR",
			"pw" => "USD",
			"py" => "PYG",
			"qa" => "QAR",
			"re" => "EUR",
			"ro" => "RON",
			"rs" => "CSD",
			"ru" => "RUB",
			"rw" => "RWF",
			"sa" => "SAR",
			"sb" => "SBD",
			"sc" => "SCR",
			"sd" => "SDG",
			"se" => "SEK",
			"sg" => "SGD",
			"sh" => "SHP",
			"si" => "EUR",
			"sj" => "NOK",
			"sk" => "EUR",
			"sl" => "SLL",
			"sm" => "EUR",
			"sn" => "XOF",
			"so" => "SOS",
			"sr" => "SRD",
			"st" => "STD",
			"su" => "SUR",
			"sv" => "USD",
			"sy" => "SYP",
			"sz" => "SZL",
			"tc" => "USD",
			"td" => "XAF",
			"tf" => "EUR",
			"tg" => "XOF",
			"th" => "THB",
			"tj" => "TJR",
			"tk" => "NZD",
			"tl" => "USD",
			"tm" => "TMT",
			"tn" => "TND",
			"to" => "TOP",
			"tr" => "TRY",
			"tt" => "TTD",
			"tv" => "AUD",
			"tw" => "TWD",
			"tz" => "TZS",
			"ua" => "UAH",
			"ug" => "UGX",
			"um" => "USD",
			"us" => "USD",
			"uy" => "UYU",
			"uz" => "UZS",
			"va" => "EUR",
			"vc" => "XCD",
			"ve" => "VEF",
			"vg" => "USD",
			"vi" => "USD",
			"vn" => "VNC",
			"vu" => "VUV",
			"wf" => "XPF",
			"ws" => "WST",
			"yd" => "YDD",
			"ye" => "YER",
			"yt" => "EUR",
			"yu" => "YUN",
			"za" => "ZAL",
			"zm" => "ZMW",
			"zr" => "CDF",
			"zw" => "USD"
		);

		if($value == 'unique')
		{
			$currencies = array_unique($currencies);
			foreach($currencies AS $key => $currency)
			{
				$values[] = $currency;
			}

			sort($values);
			$return = $values;
			//$keys = array(implode(',',$keys));
		}
		elseif($value == 'options')
		{
			foreach($currencies AS $key => $currency)
			{
				$keys[] = $key;
			}
			//$keys = array(implode(',',$keys));
			$return = $keys;
		}
		elseif($value)
		{
			$return = $currencies[$value];
		}
		else
		{
			$return = $currencies;
		}

		return $return;
	}

}
