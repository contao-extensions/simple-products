<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2025 Leo Feyer
 *
 * @package   contao-simple-products
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2025 <https://www.fast-media.net>
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'SimpleProducts',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Models
	'SimpleProducts\ProductSettingsModel'     => 'system/modules/simple-products/models/ProductSettingsModel.php',
	'SimpleProducts\ProductArchiveModel'      => 'system/modules/simple-products/models/ProductArchiveModel.php',
	'SimpleProducts\ProductCategoryModel'     => 'system/modules/simple-products/models/ProductCategoryModel.php',
	'SimpleProducts\ProductTypeModel'         => 'system/modules/simple-products/models/ProductTypeModel.php',
	'SimpleProducts\ProductModel'             => 'system/modules/simple-products/models/ProductModel.php',
	'SimpleProducts\ModuleModel'              => 'system/modules/simple-products/models/ModuleModel.php',

	// Classes
	'SimpleProducts\Currencies'               => 'system/modules/simple-products/classes/Currencies.php',
	'SimpleProducts\Product'                  => 'system/modules/simple-products/classes/Product.php',
	'SimpleProducts\ProductPrice'             => 'system/modules/simple-products/classes/ProductPrice.php',

	// Modules
	'SimpleProducts\ModuleProductList'        => 'system/modules/simple-products/modules/ModuleProductList.php',
	'SimpleProducts\ModuleCategory'           => 'system/modules/simple-products/modules/ModuleCategory.php',
	'SimpleProducts\ModuleProductReader'      => 'system/modules/simple-products/modules/ModuleProductReader.php',
	'SimpleProducts\ProductGallery'           => 'system/modules/simple-products/modules/ProductGallery.php',
	'SimpleProducts\ProductListFeatures'      => 'system/modules/simple-products/modules/ProductListFeatures.php',
	'SimpleProducts\ModuleSubscribeProduct'   => 'system/modules/simple-products/modules/ModuleSubscribeProduct.php',
	'SimpleProducts\ProductCategory'          => 'system/modules/simple-products/modules/ProductCategory.php',
	'SimpleProducts\SimpleProductsUpgrade'    => 'system/modules/simple-products/modules/SimpleProductsUpgrade.php',
	'SimpleProducts\ModuleProductRequest'     => 'system/modules/simple-products/modules/ModuleProductRequest.php',
	'SimpleProducts\ModuleCategoryList'       => 'system/modules/simple-products/modules/ModuleCategoryList.php',
	'SimpleProducts\ModuleCategoryNavigation' => 'system/modules/simple-products/modules/ModuleCategoryNavigation.php',
	'SimpleProducts\ModuleProduct'            => 'system/modules/simple-products/modules/ModuleProduct.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'category_list'            => 'system/modules/simple-products/templates/category',
	'product_full_tabs'        => 'system/modules/simple-products/templates/products',
	'product_features'         => 'system/modules/simple-products/templates/products',
	'product_full'             => 'system/modules/simple-products/templates/products',
	'product_list'             => 'system/modules/simple-products/templates/products',
	'product_simple'           => 'system/modules/simple-products/templates/products',
	'mod_product_request'      => 'system/modules/simple-products/templates/modules',
	'mod_product_list'          => 'system/modules/simple-products/templates/modules',
	'mod_product_category_list' => 'system/modules/simple-products/templates/modules',
	'mod_product_reader'        => 'system/modules/simple-products/templates/modules',
	'nav_image'                => 'system/modules/simple-products/templates/navigation',
	'j_tabs'                   => 'system/modules/simple-products/templates/jquery',
));
